import unittest
from datetime import datetime
from json import loads
from os import environ, path
from unittest.mock import patch, mock_open

import yaml

from pocket_statistics.utils import load_config, create_location_if_not_exist, save
from pocket_statistics.report import ReadWordPerDay, RemainingWordPerDay
from pocket_statistics.statistics import PocketStatistics


class UtilsTest(unittest.TestCase):

    @patch("builtins.open", new_callable=mock_open, read_data="key: value")
    def test_load_config(self, mock_file):
        load_config_result = load_config()
        self.assertEqual(load_config_result, {"key":"value"})
        mock_file.assert_called_with("./config/config.yaml")

    @patch('os.makedirs')
    @patch('os.path.exists', return_value = False)
    def test_create_location_not_exist(self, path_exist, os_makedirs):
        create_location_if_not_exist('/path/')
        os_makedirs.assert_called_once_with('/path/')

    @patch('os.makedirs')
    @patch('os.path.exists', return_value = True)
    def test_create_location_exist(self, path_exist, os_makedirs):
        create_location_if_not_exist('/path/')
        os_makedirs.assert_not_called()

    @patch("builtins.open", new_callable=mock_open)
    @patch('pocket_statistics.utils.load_config', return_value={"backup_location":"/tmp"})
    @patch('pickle.dump')
    def test_save(self, pickle_dump, utils_load_config, backup_file):
        save("key", "data")
        backup_file.assert_called_once_with("/tmp/key", 'wb')
        pickle_dump.assert_called_once_with("data", backup_file())


class StatisticsTest(unittest.TestCase):

    def setUp(self):
        pocketApi_patcher =  patch('pocket_statistics.statistics.PocketApi')
        self.api = pocketApi_patcher.start()
        self.addCleanup(pocketApi_patcher.stop)
        fixture_file_names = [
            'test_data/default.json',
            'test_data/article_add.json',
            'test_data/article_arhive.json'
        ]
        self.test_data = []
        for fixture_file in fixture_file_names:
            prefix_dir = path.dirname(__file__) # get absolute path
            fh = open(prefix_dir +'/'+ fixture_file, encoding="utf-8")
            data = loads(fh.read())
            self.test_data.append(data)
            fh.close()
        self.api().get.side_effect = [ (x, None) for x in self.test_data]
        self.api().access_token = environ["CONSUMER_KEY"]

    def get_default_statistics_object(self):
        return PocketStatistics(environ["CONSUMER_KEY"], environ["ACCESS_TOKEN"], environ["DATABASE_COLLECTION_NAME"])

    def test_inital(self):
        pocket_statistics_object = self.get_default_statistics_object()
        self.assertEqual(self.test_data[0], pocket_statistics_object.data)
    
    def test_update_article_add(self):
        final_data = self.test_data[0].copy()
        second_data_copy = self.test_data[1].copy()
        final_data["list"].update(second_data_copy.pop("list"))
        final_data.update(second_data_copy)

        pocket_statistics_object = self.get_default_statistics_object()
        pocket_statistics_object.update()
        self.assertEqual(final_data, pocket_statistics_object.data)

    def test_update_article_archive(self):
        final_data = self.test_data[0].copy()
        second_data_copy = self.test_data[1].copy()
        third_data_copy = self.test_data[2].copy()
        final_data["list"].update(second_data_copy.pop("list"))
        final_data.update(second_data_copy)
        final_data["list"].update(third_data_copy.pop("list"))
        final_data.update(third_data_copy)

        pocket_statistics_object = self.get_default_statistics_object()
        pocket_statistics_object.update()
        pocket_statistics_object.update()
        self.assertEqual(final_data, pocket_statistics_object.data)

    @patch("pocket_statistics.utils.save")
    def test_save_backup(self, utils_save):
        pocket_statistics_object = self.get_default_statistics_object()
        pocket_statistics_object.save()
        utils_save.assert_called_once_with(
            pocket_statistics_object.pocket_api.access_token,
            pocket_statistics_object
        )

    @patch("pocket_statistics.utils.load")
    def test_load_backup(self ,utils_load):
        pocket_statistics_object = PocketStatistics.load('access_token')
        self.assertEqual(pocket_statistics_object, utils_load('access_token'))

    ## RemainingWordPerDay tests

    def test_remaining_word_count_add(self):
        final_word_count = int(self.test_data[1]["list"]["73871455"]["word_count"]) + int(self.test_data[0]["list"]["2258831998"]["word_count"])
        pocket_statistics_object = self.get_default_statistics_object()
        pocket_statistics_object.update()
        pocket_statistics_object.generate_reports(reports=[RemainingWordPerDay])
        self.assertEqual(pocket_statistics_object.last_reports[0].record, {
            "date" : str(datetime.now().date()),
            "value" : final_word_count
        })

    def test_remaining_word_count_archive(self):
        final_word_count = int(self.test_data[1]["list"]["73871455"]["word_count"])
        pocket_statistics_object = self.get_default_statistics_object()
        pocket_statistics_object.update()
        pocket_statistics_object.update()
        pocket_statistics_object.generate_reports(reports=[RemainingWordPerDay])
        self.assertEqual(pocket_statistics_object.last_reports[0].record, {
            "date" : str(datetime.now().date()),
            "value" : final_word_count
        })

    @patch("pymongo.collection.Collection.insert_one")
    def test_remaining_word_count_save(self, mock_insert):
        pocket_statistics_object = self.get_default_statistics_object()
        pocket_statistics_object.update()
        pocket_statistics_object.generate_reports(reports=[RemainingWordPerDay])
        pocket_statistics_object.save_reports(environ["MONGODB_HOST"], environ["MONGODB_PORT"])
        mock_insert.assert_called_once_with(pocket_statistics_object.last_reports[0].record)

    ## ReadWordPerDay tests

    def test_read_word_count_add(self):
        pocket_statistics_object = self.get_default_statistics_object()
        pocket_statistics_object.update()
        pocket_statistics_object.generate_reports(reports=[ReadWordPerDay])
        self.assertEqual(pocket_statistics_object.last_reports[0].record, {
            "date" : str(datetime.now().date()),
            "value" : 0
        })

    def test_read_word_count_archive(self):
        final_word_count = int(self.test_data[0]["list"]["2258831998"]["word_count"])
        pocket_statistics_object = self.get_default_statistics_object()
        pocket_statistics_object.update()
        pocket_statistics_object.update()
        pocket_statistics_object.generate_reports(reports=[ReadWordPerDay])
        self.assertEqual(pocket_statistics_object.last_reports[0].record, {
            "date" : str(datetime.now().date()),
            "value" : final_word_count
        })

    @patch("pymongo.collection.Collection.insert_one")
    def test_read_word_count_save(self, mock_insert):
        pocket_statistics_object = self.get_default_statistics_object()
        pocket_statistics_object.update()
        pocket_statistics_object.generate_reports(reports=[ReadWordPerDay])
        pocket_statistics_object.save_reports(environ["MONGODB_HOST"], environ["MONGODB_PORT"])
        mock_insert.assert_called_once_with(pocket_statistics_object.last_reports[0].record)


# if __name__ == '__main__':
#     unittest.main()
