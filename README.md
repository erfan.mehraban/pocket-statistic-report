# Pocket Statistics Report Generator

[![coverage report](https://gitlab.com/erfan.mehraban/pocket-statistic-report/badges/master/coverage.svg)](https://gitlab.com/erfan.mehraban/pocket-statistic-report/commits/master)

[![pipeline status](https://gitlab.com/erfan.mehraban/pocket-statistic-report/badges/master/pipeline.svg)](https://gitlab.com/erfan.mehraban/pocket-statistic-report/commits/master)


This is microservice based application to generate and view [pocket app](getpocket.com) statistics. You can view some plot like read rate and remaining article by using this app.

**NOTE:This project is under development so some functionality may lack.**

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

Just insall Docker and Docker Compose to deploy app.

### Deployment

```bash
docker-compose up -d
```

### use application

**UNSER CUNSTRUCTION**

## Running the tests

```
coverage run -m unittest discover
```


## Concept and Design

![Design](https://gitlab.com/erfan.mehraban/pocket-statistic-report/raw/master/Desing.png)

## Configure

**NOTHING FOR NOW!**

## Built With

* Python3
* Django Rest Framwork
* Redis
* Docker-Compose
* rabbitMQ
* monogoDB

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details

## TODO

...