FROM python:latest

LABEL version=0.1

ADD requirements.txt /app/
ADD pocket_statistics/ /app/pocket_statistics/
ADD main/schedule.py /app/
WORKDIR /app

RUN python3 -m pip install git+https://github.com/erfan-mehraban/pocket
RUN python3 -m pip install -r requirements.txt

CMD ["python3", "schedule.py"]