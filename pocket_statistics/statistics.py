import logging

from pocket import AuthException
from pocket import Pocket as PocketApi
from pymongo import MongoClient

from . import report
from . import utils

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

class PocketStatistics:

    all_reports = [value for key, value in report.__dict__.items()
                    if report.BaseReport in getattr(value, '__bases__',[])]

    def __init__(self, consumer_key, access_token=None, database_collection_name="pocket_statistics"):
        if access_token is None:
            access_token = PocketApi.auth(consumer_key)
        try:
            self.pocket_api = PocketApi(consumer_key, access_token)
        except AuthException as e:
            logger.info("authorize failed: {}".format(e.args))
            # TODO: better mechanism for this situation
            raise e
        logger.info("authorized correctly")
        self.database_collection_name = database_collection_name
        self.last_update = '0'
        self.data = self._get_updates()
        logger.info("data initialized")

    def _get_updates(self):
        content, header = self.pocket_api.get(detailType="simple", since=self.last_update)
        self.last_update = content["since"]
        return content

    def update(self):
        new_data = self._get_updates()
        logger.info("new update recieved")
        articles = new_data.pop('list')
        self.data.update(new_data)
        self.data["list"].update(articles)

    def generate_reports(self, reports=all_reports):
        aggregator = report.Aggregator(self.data)
        self.last_reports = []
        for report_class in reports:
            report_object = report_class(self.data, aggregator)
            logger.info("{} report generated".format(report_class.__name__))
            self.last_reports.append(report_object)

    def save_reports(self, host, port):
        database = MongoClient(host, int(port))[self.database_collection_name]
        for r in self.last_reports:
            collection = database[r.get_collection_name()]
            record = r.get_record()
            record["user"] = self.pocket_api.access_token # TODO: user username (pocket api should be change too)
            collection.insert_one(record)
            logger.info("report saved in {}".format(r.get_collection_name()))

    def save(self):
        #TODO: use user_id
        utils.save(self.pocket_api.access_token, self)
        logger.info("pocket statistic saved with this key : {}".format(self.pocket_api.access_token))
    
    @classmethod
    def load(cls, access_token):
        logger.info("pocket statistic loaded by : {}".format(access_token))
        #TODO: use user_id
        return utils.load(access_token)
