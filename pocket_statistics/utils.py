import json
import os

import yaml
import pickle


def load_config():
    with open("./config/config.yaml") as config_file:
        return yaml.load(config_file.read())

def create_location_if_not_exist(path):
    if not os.path.exists(path):
        os.makedirs(path)

def save(key, data):
    backup_location = load_config()["backup_location"]
    if not backup_location.endswith("/"):
        backup_location = backup_location + "/"
    create_location_if_not_exist(backup_location)
    with open(backup_location+key, "wb") as backup_file:
        pickle.dump(data, backup_file)

def load(key):
    backup_location = load_config()["backup_location"]
    if not backup_location.endswith("/"):
        backup_location = backup_location + "/"
    try:
        with open(backup_location+key, 'rb') as backup_file:
            return pickle.load(backup_file)
    except FileNotFoundError:
        raise KeyError(f"no backup with this key: {key}")