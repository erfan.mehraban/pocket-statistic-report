from datetime import datetime

class BaseReport:
    record = {}
    
    def __init__(self, data, aggregator):
        raise NotImplementedError()

    def get_record(self):
        return self.record
    
    def get_collection_name(self):
        return self.__class__.__name__.lower()

    def today(self):
        return str(datetime.now().date())


class RemainingWordPerDay(BaseReport):
    def __init__(self, data, aggregator):
        self.record = {
            'date' : self.today(),
            'value' : aggregator.unread_word_sum
        }

class ReadWordPerDay(BaseReport):
    def __init__(self, data, aggregator):
        self.record = {
            'date' : self.today(),
            'value' : aggregator.read_word_sum
        }


class Aggregator:
    unread_word_sum = 0
    read_word_sum = 0

    def __init__(self, data):
        for article_id, article in data["list"].items():
            self.count_unread_words(article)
            self.count_read_words(article)

    def get_filtered_article_word(self, article, status):
        """ filter article with given status and return word count """
        if article.get('status', None) == status:
            wc = article.get('word_count', 0)
            return int(wc)
        return 0

    def count_unread_words(self, article):
        self.unread_word_sum += self.get_filtered_article_word(article, '0')
    
    def count_read_words(self, article):
        self.read_word_sum += self.get_filtered_article_word(article, '1')
