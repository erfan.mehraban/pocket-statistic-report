import sched
from os import environ

from pocket_statistics.statistics import PocketStatistics

ps = PocketStatistics(environ["CONSUMER_KEY"], environ["ACCESS_TOKEN"], environ["DATABASE_COLLECTION_NAME"])
def run_get_statistics_cycle():
    ps.update()
    ps.generate_reports()
    ps.save_reports(environ["MONGODB_HOST"], environ["MONGODB_PORT"])

one_day_in_second = 24 * 60 * 60
scheduler = sched.scheduler()
if __name__ == "__main__":
    while True:
        e = scheduler.enter(one_day_in_second, 1, run_get_statistics_cycle)
        scheduler.run()
